# RidesBikes

RideBikes est un site internet proposant de partager des sorties vélo, pour rassembler au maximum les pratiquants

## Environnement de développement

### Pré-requis

* PHP 8
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-requis (sauf Docker er Docker-compose) avec cette commande de la Symfony CLI 
```bash
symfony check:requirements
```

### Lancer l'environnnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Ajouter des données de tests

```bash
symfony console doctrine:fixtures:load
```

## Lancer des tests


```bash
php bin/phpunit --testdox
```
