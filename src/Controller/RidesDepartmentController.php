<?php

namespace App\Controller;

use App\Repository\RideRepository;
use App\Repository\DepartmentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RidesDepartmentController extends AbstractController
{
    #[Route('/ridesdepartment/{department}', name: 'app_rides_department')]
    public function index( PaginatorInterface $paginator, $department, Request $request, EntityManagerInterface $entityManager): Response
    {
        $data = $this->$entityManager->getRepository(Department::class)->findByDepartment($department, ['createdAt' => 'DESC']); 

        $ride = $paginator->paginate($data, $request->query->getInt('page', 1), 6);

        if (!$ride) {
            throw $this->createNotFoundException('Aucune sortie trouvé');
        }


        return $this->render('rides_department/index.html.twig', [
            'data' => $data
        ]);
    }
}
