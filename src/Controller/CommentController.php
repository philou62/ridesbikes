<?php

namespace App\Controller;

use App\Entity\Ride;
use App\Form\CommentFormType;
use App\Entity\Comment;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    #[Route('/ride/{id<[0-9]+>}', name: 'app_ride_show', methods: ['get', 'post'])]
    public function show(Ride $ride, EntityManagerInterface $em, Request $request, CommentRepository $repo): Response
    {
        $comment = new Comment();

        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setRide($ride);

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('app_ride_show', ['id' => $ride->getId()]);
        }

        return $this->renderForm('ride/show.html.twig', [
            'ride' => $ride,
            'commentForm' => $form
        ]);
    }
}
