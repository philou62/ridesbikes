<?php

namespace App\Controller;

use App\Entity\Ride;
use App\Entity\Comment;
use App\Form\CreateRideType;
use App\Form\CommentFormType;
use App\Repository\RideRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RideController extends AbstractController
{
    #[Route('/ride', name: 'app_ride', methods: ['GET'])]
    public function index(RideRepository $rideRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $data = $rideRepository->findBy([], ['createdAt' => 'DESC']);

        $ride = $paginator->paginate($data, $request->query->getInt('page', 1), 12);


        return $this->render('ride/index.html.twig', compact('ride'));
    }

    

    #[Route('/ride/create', name: 'app_ride_create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $ride = new Ride;
        $form = $this->createForm(CreateRideType::class, $ride);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ride->setUser($this->getUser());
            $em->persist($ride);
            $em->flush();

            $this->addFlash('success', 'La sortie à éte créee avec succés');
            return $this->redirectToRoute('app_ride');
        }
        return $this->renderForm('ride/create.html.twig', [
            'formRideCreate' => $form,
        ]);
    }

    #[Route('/ride/{id<[0-9]+>}/edit', name: 'app_ride_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Ride $ride, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CreateRideType::class, $ride);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($ride);
            $em->flush();


            $this->addFlash('success', 'La sortie à éte modifiée avec succés');
            return $this->redirectToRoute('app_ride');
        }

        return $this->renderForm('ride/edit.html.twig', [
            'ride' => $ride,
            'formRideCreate' => $form
        ]);
    }

    #[Route('/ride/{id<[0-9]+>}/delete', name: 'app_ride_delete', methods: ['POST'])]
    public function delete(Request $request, Ride $ride, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid(
            'ride_deletion_' . $ride->getId(),
            $request->request->get('csrf_token')
        )) {
            $em->remove($ride);
            $em->flush();

            $this->addFlash('info', 'la sortie à été supprimé avec succés');
        }
        return $this->redirectToRoute('app_ride');
    }
}
