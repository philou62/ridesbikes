<?php

namespace App\Form;

use App\Entity\Ride;
use Doctrine\ORM\Cache\Region;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CreateRideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nom de la sortie',
                    'attr' => [
                        'placeholder' => 'Veuillez donner le titre de la sortie'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de la sortie',
                'attr' => [
                    'placeholder' => 'Veuillez donner une description de la sortie'
                    ]
            ])
            ->add('mileage', IntegerType::class, [
                'label' => 'Nombre de kms',
                'attr' => [
                    'placeholder' => 'Veuillez donner le Nombre de kilométres de la sortie'
                    ]
            ])
            ->add('positiveElevation', IntegerType::class, [
                'label' => 'Denivelé positif',
                'attr' => [
                    'placeholder' => 'Veuillez donner le Dénivelé positif de la sortie'
                    ]
            ])
            ->add('departureCity', TextType::class, [
                'label' => 'Ville de départ',
                'attr' => [
                    'placeholder' => 'Veuillez donner la ville de départ de la sortie'
                    ]
            ])
            ->add('cityOfArrival', TextType::class, [
                'label' => 'Ville d\'arrivée',
                'attr' => [
                    'placeholder' => 'Veuillez donner la ville d\'arrivée de la sortie'
                    ]
            ])
            ->add('departureTime', TimeType::class, [
                'label' => 'Heure de départ',
                'data' => new \DateTime(),
                'attr' => [
                    'placeholder' => 'Veuillez donner l\'Heure de départ de la sortie'
                ]
            ])
            ->add('Date', DateType::class, [
                'label' => 'Veuillez donner la date de la sortie',
                'data' => new \DateTime()
            ])
            ->add('department')

            ->add('category');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ride::class, Region::class
        ]);
    }
}
