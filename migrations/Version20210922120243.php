<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210922120243 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ride_note (ride_id INT NOT NULL, note_id INT NOT NULL, INDEX IDX_8482CF60302A8A70 (ride_id), INDEX IDX_8482CF6026ED0855 (note_id), PRIMARY KEY(ride_id, note_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ride_note ADD CONSTRAINT FK_8482CF60302A8A70 FOREIGN KEY (ride_id) REFERENCES ride (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ride_note ADD CONSTRAINT FK_8482CF6026ED0855 FOREIGN KEY (note_id) REFERENCES note (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD ride_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C302A8A70 FOREIGN KEY (ride_id) REFERENCES ride (id)');
        $this->addSql('CREATE INDEX IDX_9474526C302A8A70 ON comment (ride_id)');
        $this->addSql('ALTER TABLE ride ADD user_id INT NOT NULL, ADD department_id INT NOT NULL, ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD0AE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD012469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_9B3D7CD0A76ED395 ON ride (user_id)');
        $this->addSql('CREATE INDEX IDX_9B3D7CD0AE80F5DF ON ride (department_id)');
        $this->addSql('CREATE INDEX IDX_9B3D7CD012469DE2 ON ride (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ride_note');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C302A8A70');
        $this->addSql('DROP INDEX IDX_9474526C302A8A70 ON comment');
        $this->addSql('ALTER TABLE comment DROP ride_id');
        $this->addSql('ALTER TABLE ride DROP FOREIGN KEY FK_9B3D7CD0A76ED395');
        $this->addSql('ALTER TABLE ride DROP FOREIGN KEY FK_9B3D7CD0AE80F5DF');
        $this->addSql('ALTER TABLE ride DROP FOREIGN KEY FK_9B3D7CD012469DE2');
        $this->addSql('DROP INDEX IDX_9B3D7CD0A76ED395 ON ride');
        $this->addSql('DROP INDEX IDX_9B3D7CD0AE80F5DF ON ride');
        $this->addSql('DROP INDEX IDX_9B3D7CD012469DE2 ON ride');
        $this->addSql('ALTER TABLE ride DROP user_id, DROP department_id, DROP category_id');
    }
}
